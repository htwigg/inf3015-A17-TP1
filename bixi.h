/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2017 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

/*
 COMPLÈTÉ PAR:
    1) Lou-Gomes Neto, NETL14039105
    2) Hugo Twigg-Coté, TWIH25048700
 */

#if !defined(_BIXI__H_)
#define _BIXI__H_

#include <iostream>
#include "pointst.h"
#include "tableau.h"

using namespace std;

// Représente une station BIXI
class Station
{
public:
  // Interface publique ici.
  Station() {}
  Station(int id_, string nom_, PointST point_, int nbVelos_, int ptsAncrages_);
  ~Station();
  string getNom();
  PointST getCoord();
  int getNbVelos();
  int getPtsAncrages();
  void prendreVelo();
  void deposerVelo();

private:
  // Représentation ici.
  int id;
  string nom;
  PointST point;
  int nbVelos;
  int ptsAncrages;

  friend ostream &operator<<(ostream &os, const Station &station);
  friend istream &operator>>(istream &is, Station &station);
};

// Représente un réseau de stations BIXI
class Reseau
{
public:
  // Interface publique ici.
  Reseau();
  ~Reseau();
  void calculerDistancesStations();
  void afficherTrajetVelo(int station1, int station2, double temps);
  void afficherTrajetMarche(double distanceMarche);
  void mettreAJourStations(int station1, int station2);
  void traiterRequete(PointST &p1, PointST &p2);
  
private:
  // Représentation ici.
  Tableau<Station> stations;
  Tableau<double> distanceStations; //tous les possibilites de distances entre les stations

  friend istream &operator>>(istream &is, Reseau &reseau);
  friend ostream &operator<<(ostream &os, const Reseau &reseau);
};

#endif
