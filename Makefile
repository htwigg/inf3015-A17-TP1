# Makefile pour TP1.

# COMPLÈTÉ PAR: Lou-Gomes Neto, NETL14039105 
# et Hugo Twigg-Coté, TWIH25048700

# Choisir l'une des deux configurations (-g: Debug | -O2: Release)
#OPTIONS = -g -O0 -Wall
OPTIONS = -O2 -Wall

all : tp1

tp1 : tp1.cpp pointst.o bixi.o
	g++ ${OPTIONS} -o tp1 tp1.cpp pointst.o bixi.o
	
pointst.o : pointst.cpp pointst.h tableau.h
	g++ ${OPTIONS} -c -o pointst.o pointst.cpp

bixi.o : bixi.cpp bixi.h pointst.h tableau.h
	g++ ${OPTIONS} -c -o bixi.o bixi.cpp

clean:
	rm -rf tp1 *~ *.o

