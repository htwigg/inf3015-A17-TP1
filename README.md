# **Travail pratique #1 :**
# **Système de recommandation pour les usagers de BIXI**

### **1. Objectifs**

* S'initier à la programmation en C++.
* Pratiquer l'encapsulation et concevoir des types abstraits de données.
* Gérer la mémoire.
* Implémenter et appliquer des structures de données simples en C++.
* Résoudre un problème simple.
* Analyser la complexité temporelle d'un algorithme.

### **2. Problématique**

Vous devez écrire un programme C++ nommé tp1 qui assiste des usagers du BIXI, un système de location de vélos en libre-service. Le programme reçoit en entrée un état du réseau BIXI et liste de requêtes de déplacements à servir. Pour chaque requête, le programme affiche une recommandation. Il a 2 types de recommandations possibles :

1. marcher jusqu'à une station BIXI, prendre un vélo, pédaler jusqu'à une autre station, et finalement marcher jusqu'à la destination finale;
2. ou marcher directement jusqu'à la destination, et ce, sans utiliser de vélo.

### **2.1 Critère de recommandation et hypothèses simplificatrices**

Votre programme doit recommander l'option la plus rapide en temps (nombre de secondes). Pour cela, on considère qu'un usager marche à une vitesse constante de 1 mètre par seconde (1 m/s), et roule à BIXI à une vitesse constante de 4 mètres par seconde (4 m/s). Afin de simplifier le problème, on suppose que les déplacements se font en ligne droite sur la surface la Terre. La distance entre deux point (coordonnées latitude et longitude) sur la carte se calcule avec la fonction PointST::distance() fournie. Celle-ci retourne la longueur en mètres en faisant l'hypothèse que la Terre est une sphère parfaite d'un rayon de 6371 km. Pour l'instant, les routes et les obstacles ne doivent pas être considérés. Nous verrons comment considérer les routes plus tard dans le cours (partie graphes).

Exemple 1

À titre d'exemple, voici une portion de la carte de Montréal avec quelques stations BIXI
![alt text][logo]

[logo]: http://ericbeaudry.uqam.ca/INF3105/tp1/reseau1.png "Carte Réseau Bixi"

Supposons qu'un usager soit initialement localisé au pavillon **Président-Kennedy de l'UQAM**, soit aux coordonnées (45.509444, -73.568304). L'usager désire se rendre à la **Grande Bibliothèque**, soit aux coordonnées (45.515399, -73.561996). Pour cette requête, la meilleure option de déplacement est de :

1. marcher jusqu'à la station Clark / Evans (17 vélos disponibles), et y prendre un vélo;
2. pédaler jusqu'à la station Berri / de Maisonneuve (9 points d'ancrage disponibles), et y ancrer le vélo;
3. marcher jusqu'à la destination finale

### **2.3 Exemple 2**
Supposons qu'un autre usager soit localisé au **pavillon Adrien-Pinard de l'UQAM**, soit aux coordonnées (45.510843, -73.569871). L'usager désire se rendre au restaurant **Cégep du Vieux Montréal**, soit aux coordonnées (45.514241, -73.567339). Pour cette requête, la meilleure option de déplacement est de marcher sans emprunter de vélo, car la station de l'Hôtel de Ville / Sherbrooke n'a aucun point d'ancrage disponible.

### **2.4 Traitement séquentiel des requêtes**
les requêtes doivent être traitées séquentiellement, c'est-à-dire qu'on suppose qu'il y a toujours qu'un seul déplacement à la fois.


Après chaque requête, le programme doit mettre à jour l'état du réseau BIXI selon la recommandation effectuée. La mise à jour de l'état est nécessaire pour répondre aux requêtes subséquentes. Par exemple, suite à la requête de l'exemple 1, il y aura 16 vélos et 2 points d'ancrage disponibles à la station Clark / Evans, et il y aura 23 vélos et 8 points d'ancrage disponibles à la station Berri / de Maisonneuve.


Les recommandations sont optimisées sur une base individuelle, c'est-à-dire qu'on ne considère que la requête courante. On ne doit pas considérer les requêtes suivantes. Par exemple, si on combine les exemples 1 et 2 ci-haut, un meilleur plan global aurait pu être de recommander au premier usager de rendre son vélo à University/des Pins, plutôt qu'à la station Hutchison/des Pins, afin qu'il soit disponible pour le deuxième usager. Effectuer de telles optimisations dépasse les objectifs du cours.


### **3. Structure du programme**

Pour bien amorcer ce travail, il est fortement recommandé de commencer avec le squelette de départ fourni dans tp1.zip. Vous pouvez modifier ces fichiers autant que vous désirez. Toutefois, vous devez préserver la syntaxe d'appel du programme et ses formats d'entrée et de sortie. Cela est nécessaire pour la correction automatique.

### **3.1 Syntaxe d'appel du programme tp1**

Le programme tp1 doit pouvoir être lancé en ligne de commande avec l'une des deux syntaxes suivantes :

``` ./tp1 reseau-bixi.txt requetes.txt ```

``` ./tp1 reseau-bixi.txt < requetes.txt ```


où :


* le fichier reseau-bixi.txt contient un état d'un réseau BIXI, soit une liste des stations avec le nombre de vélos et de points d'ancrage disponibles, et;
* le fichier requetes.txt contient une liste de requêtes.

Si requetes.txt est passé en argument (argv[2]), alors votre programme doit lire depuis le fichier nomfichier au moyen d'un flux de lecture de type std::ifstream. Sinon, votre programme doit lire dans l'entrée standard (stdin) au moyen du flux d'entrée std::cin. À noter que le squelette fourni implémente déjà cela.

Les résultats produits par votre programme doivent être écrits dans la sortie standard (stdout) à l'aide du flux de sortie _C++ std::cout_.

### **3.2 Fichier d'état d'un réseau BIXI**

Un fichier de réseau BIXI est structuré de la façon suivante. Sur chaque ligne, on y retrouve une station BIXI spécifiée par les champs suivants :

* ID (inutilisé pour le TP1);
* nom;
* point spécifié par les coordonnées (longitude,latitude);
* nombre de vélos disponibles;
* nombre de points d'ancrage disponibles;

Pour faciliter la lecture (le parsing) au moyen d'un flux de lecture [std::istream](http://en.cppreference.com/w/cpp/io/basic_istream), des tabulations ou des espaces blancs séparent les champs. De plus, les noms de station ne contiennent aucun espace blanc. Ainsi, ils peuvent être lus au moyen d'un seul appel à l'opérateur >>.

À titre d'exemple, voici un extrait du fichier d'entrée [montreal-bixi.txt](http://ericbeaudry.uqam.ca/INF3105/tp1/montreal-bixi.txt) fourni.


``` 
1	Hotel-de-Ville_2_(du_Champs-de-Mars_/_Gosford)	(45.509310285552,-73.554431051016)	13	2
2	Ste-Catherine_/_Dezery	(45.539230296982,-73.541082367301)	4	19
3	Clark_/_Evans	(45.511132280739,-73.567907139659)	17	1
4	Hotel-de-Ville_(du_Champs-de-Mars_/_Gosford)	(45.509228519662,-73.554469943047)	13	22
5	Metcalfe_/_Square_Dorchester	(45.500233546666,-73.571126461029)	29	2
6	18e_avenue_/_Rosemont	(45.557895457529,-73.576529100537)	1	22
7	de_l'Hotel-de-Ville_/_Ste-Catherine	(45.511640093571,-73.562183976173)	10	13
8	Sanguinet_/_Ste-Catherine	(45.512734,-73.5611406)	25	10
9	Ste-Catherine_/_Labelle	(45.515051758517,-73.559221476316)	14	9
10	Ste-Catherine_/_St-Denis	(45.513833539884,-73.560445904732)	3	16
11	St-Andre_/_St-Antoine	(45.5140873,-73.55230004)	16	15
12	Metro_St-Laurent_(de_Maisonneuve_/_St-Laurent)	(45.51066,-73.56497)	35	14
13	Sanguinet_/_de_Maisonneuve	(45.513542213035,-73.562870621681)	12	19
14	St-Denis_/_de_Maisonneuve	(45.514395514513,-73.561765551567)	16	11
15	Berri_/_de_Maisonneuve	(45.515299,-73.561273)	22	9
...
```
### **3.3 Fichier de requetes**

Chaque requête est spécifiée sur une seule ligne. Une requête est spécifiée par un point d'origine et un point de destination.

À titre d'exemple, voici le fichier [montreal-req0.txt](http://ericbeaudry.uqam.ca/INF3105/tp1/montreal-req0.txt). Ce dernier contient les deux requêtes des exemples 1 et 2 des sections 2.2 et 2.3 ci-haut
```
(45.509444, -73.568304) (45.515399, -73.561996)
(45.510843, -73.569871) (45.514241, -73.567339)
```
### **3.4 Sortie**

Les recommandations calculées par le programme doivent être écrites dans la sortie standard (_stdout_) à l'aide du flux de sortie _C++ std::cout_. Chaque recommandation est écrite sur une seule ligne.

Le format de sortie varie selon le type de recommandation :

* **Si la recommandation consiste à utiliser un vélo**, alors le programme doit afficher : le nom de la station où prendre un vélo, un espace, la chaine "-->", un espace, le nom de la station où rendre le vélo, un espace, la durée de déplacement en secondes, un espace et le caractère 's'.
* **Si la recommandation consiste à seulement marcher sans utiliser de vélo**, alors : le programme doit afficher : la chaine "Marche", un espace, la durée de déplacement en secondes, un espace et le caractère 's'.

Il est très important de respecter ce format de sortie, car des scripts de correction seront utilisés. Les durées affichées doivent être arrondies à la seconde près à l'aide de la fonction [round()](http://en.cppreference.com/w/c/numeric/math/round).

Durant le développement, il est recommandé d'utiliser le flux standard d'erreur C++ std::cerr pour afficher des messages de débogage. Ainsi, si vous oubliez d'enlever des messages de débogage, ces derniers ne seront pas redirigés dans le fichier de sortie.

L'exécution de la commande
```
./tp1 montreal-bixi.txt montreal-req0.txt
```
doit produire le résultat [montreal-req0+.txt](http://ericbeaudry.uqam.ca/INF3105/tp1/montreal-req0+.txt) :
```
Clark_/_Evans --> Berri_/_de_Maisonneuve 421 s
Marche 426 s
``` 

### **3.5 Algorithme**

Voici le pseudocode partiel (à compléter) d'un algorithme suggéré pour effectuer une recommandation utilisant exactement 1 vélo.

```
Entrées : origine, destination;

meilleur_temps = +infini;
Pour toutes les stations i :
    Pour toutes les stations j :
        Si la station i a aucun vélo disponible, ignorer;
        Si la station j a aucun point d'ancrage disponible, ignorer;
        tempsMarche1 = distance(origine, stations[i].coor) / vitesse_marche;
        tempsVelo = distance(stations[i].coor, stations[j].coor) / vitresse_velo;
        tempsMarche2 = distance(stations[j].coor, destination) / vitesse_marche;
        temps = tempsMarche1 + tempsVelo + tempsMarche2;
        si temps < meilleur_temps :
            meilleur_temps = temps;
            (sol1, sol2) = (i,j);
Afficher stations[sol1].nom, " --> ", stations[sol2].nom, " ", meilleur_temps, " s";
```