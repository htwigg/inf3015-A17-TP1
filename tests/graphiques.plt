# INF3105 - Structures de données et algorithmes
# UQAM / Département d'informatique
# Auteur : Eric Beaudry
#
# Graphiques pour le TP1.
# Ce script est destiné à être exécuté par GnuPlot.
# gnuplot graphiques.plt

set terminal png
set output 'graphique1.png'

set grid
set xlabel "Nb de requetes"
set ylabel "Temps CPU (s)"
set key left top box
set xrange [0:5000]
#set yrange [0.0:180]
#set log y

plot 'montreal-stats.txt' using 2:3 title "Montreal" with linespoints lw 5,\
     'toronto-stats.txt' using 2:3 title "Toronto" with linespoints lw 5,\
     'ottawa-stats.txt' using 2:3 title "Ottawa" with linespoints lw 5,\
     'london-stats.txt' using 2:3 title "Londres" with linespoints lw 5

set terminal pdf enhanced color #  size 2.5,1.8
set output 'graphique1.pdf'

plot 'montreal-stats.txt' using 2:3 title "Montreal" with linespoints lw 5,\
     'toronto-stats.txt' using 2:3 title "Toronto" with linespoints lw 5,\
     'ottawa-stats.txt' using 2:3 title "Ottawa" with linespoints lw 5,\
     'london-stats.txt' using 2:3 title "Londres" with linespoints lw 5

set terminal png
set output 'graphique2.png'

set grid
set xlabel "Nb de stations"
set ylabel "Temps CPU (s)"
set key off
set xrange [0:750]
#set yrange [0.0:180]

plot 'stats.txt' using 2:4 with linespoints lw 5

set terminal pdf enhanced color #  size 2.5,1.8
set output 'graphique2.pdf'

plot 'stats.txt' using 2:4 with linespoints lw 5


