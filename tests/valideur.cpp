/* INF3105 - Structures de données et algorithmes
   UQAM / Département d'informatique
   Automne 2016
   Valideur pour TP1
   Auteur : Eric Beaudry (beaudry.eric@uqam.ca - http://ericbeaudry.uqam.ca)

   Pour compiler :
     g++ -O valideur.cpp -o valideur
*/

#include <iostream>
#include <cctype>
#include <fstream>
using namespace std;

// Fonction pour tester l'équivalence de deux string sans tenir compte des espaces blancs (' ' et '\t').
bool stringsequivalentes(const string& s1, const string& s2){
    unsigned int i1=0;
    unsigned int i2=0;
    while(true){
        while(s1[i1]==' ' || s1[i1]=='\t') ++i1;
        while(s2[i2]==' ' || s2[i2]=='\t') ++i2;
        if(i1>=s1.size()) break;
        if(i2>=s2.size()) break;
        if(s1[++i1] != s2[++i2]) return false;
        //if(tolower(s1[++i1]) != tolower(s2[++i2])) return false;
    }
    return i1>=s1.size() && i2>=s2.size();
}

int main(int argc, const char** argv){
    // Validation du nombre de paramètres
    if(argc<3){
        cout << "./valideur solution.txt votreresultat.txt" << endl;
        return 1;
    }
    
    // Ouvertures des fichiers
    ifstream isolution(argv[1]);
    if(isolution.fail()) {
        cout << "Erreur " << argv[1] << endl;
        return 2;
    }
    ifstream iresultat(argv[2]);
    if(iresultat.fail()) {
        cout << "Erreur " << argv[2] << endl;
        return 3;
    }
    
    // Compteurs
    int nbRequetes=0, nbBonsResultats=0;
    
    // Boucle principale : lire ligne par ligne les fichiers solution et résultat, et comparer
    while(isolution && !isolution.eof()){
        string ligne_solution, ligne_resultat;
        getline(isolution, ligne_solution);
        if(ligne_solution.empty()) break;
        getline(iresultat, ligne_resultat);
        
        nbRequetes++;
        if(stringsequivalentes(ligne_solution, ligne_resultat))
        //if(ligne_solution==ligne_resultat)
            nbBonsResultats++;
    }
    
    // Affichage du résultat
    cout << nbBonsResultats
    //     << "\t/" << nbRequetes
         << "\t" << (nbBonsResultats==nbRequetes ? "OK" : "Echec")
         << endl;
    return 0;
}

