/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2017 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

/*
 COMPLÈTÉ PAR:
    1) Lou-Gomes Neto, NETL14039105
    2) Hugo Twigg-Coté, TWIH25048700
 */

#include "bixi.h"
#include "math.h"
#define MARCHE_VITESSE 1.0
#define BIXI_VITESSE 4.0
#define INFINI 1.79769e+308

// STATION

Station::Station(int id_, string nom_, PointST point_, int nbVelos_, int ptsAncrages_)
{
    id = id_;
    nom = nom_;
    point = point_;
    nbVelos = nbVelos_;
    ptsAncrages = ptsAncrages_;
}

Station::~Station()
{
}

string Station::getNom()
{
    return nom;
}

PointST Station::getCoord()
{
    return point;
}

int Station::getNbVelos()
{
    return nbVelos;
}

int Station::getPtsAncrages()
{
    return ptsAncrages;
}

void Station::prendreVelo()
{
    --nbVelos;
    ++ptsAncrages;
}

void Station::deposerVelo()
{
    ++nbVelos;
    --ptsAncrages;
}

ostream &operator<<(ostream &os, const Station &station)
{
    os << station.id
       << station.nom
       << station.point
       << station.nbVelos
       << station.ptsAncrages;
    return os;
}

istream &operator>>(istream &is, Station &station)
{
    is >> station.id 
       >> station.nom 
       >> station.point 
       >> station.nbVelos 
       >> station.ptsAncrages;
    return is;
}

// RESEAU

Reseau::Reseau()
{
    Tableau<Station> stations;
    Tableau<double> distanceStations;
}

Reseau::~Reseau()
{
}

void Reseau::calculerDistancesStations()
{
    for (int i = 0; i < stations.taille(); i++)
    {
        for (int j = 0; j < stations.taille(); j++)
        {
            distanceStations.ajouter(stations[i].getCoord().distance(stations[j].getCoord()) / BIXI_VITESSE);
        }
    }
}

void Reseau::afficherTrajetVelo(int station1, int station2, double temps)
{
    std::cout << stations[station1].getNom()
              << " --> " << stations[station2].getNom()
              << " " << round(temps) << " s" << endl;

    mettreAJourStations(station1, station2);
}

void Reseau::afficherTrajetMarche(double distanceMarche)
{
    std::cout << "Marche "
              << round(distanceMarche)
              << " s"
              << std::endl;
}

void Reseau::mettreAJourStations(int station1, int station2)
{
    stations[station1].prendreVelo();
    stations[station2].deposerVelo();
}

/**
 * Trouve un trajet optimal a partir de 2 points et affiche la trajet.
 **/ 
void Reseau::traiterRequete(PointST &p1, PointST &p2)
{
    int tailleReseau = stations.taille();
    int station1 = 0, station2 = 0;

    double meilleurTemps = INFINI;
    double distanceMarche = p1.distance(p2) / MARCHE_VITESSE;
    double tempsMarche1, tempsVelo, tempsMarche2, temps;

    Tableau<double> marcheDOrigine;
    Tableau<double> marcheADestination;

    // Precalcule des distances de marche pour: 
    // Origine a station1 et station2 a destination
    for (int i = 0; i < tailleReseau; i++)
    {
        marcheDOrigine.ajouter(p1.distance(stations[i].getCoord()) / MARCHE_VITESSE);
        marcheADestination.ajouter(stations[i].getCoord().distance(p2) / MARCHE_VITESSE);
    }

    // Recherche du trajet
    for (int i = 0; i < tailleReseau; i++)
    {
        for (int j = 0; j < tailleReseau; j++)
        {
            if (distanceMarche > (tempsMarche1 = marcheDOrigine[i]) 
            && distanceMarche > (tempsMarche2 = marcheADestination[j]))
            {
                if (stations[i].getNbVelos() != 0 
                && stations[j].getPtsAncrages() != 0)
                {
                    tempsVelo = distanceStations[i * tailleReseau + j];
                    temps = tempsMarche1 + tempsVelo + tempsMarche2;

                    if (temps < meilleurTemps)
                    {
                        meilleurTemps = temps;
                        station1 = i;
                        station2 = j;
                    }
                }
            }
        }
    }

    if (distanceMarche > meilleurTemps)
    {   
        // Affiche un trajet avec bixi, si c'est plus rapide
        afficherTrajetVelo(station1, station2, meilleurTemps);
    }
    else 
    {
        // Affiche la distance de marche, si cest plus vite
        afficherTrajetMarche(distanceMarche);
    }
}// Fin traiterRequete()

ostream &operator<<(ostream &os, const Reseau &reseau)
{
    for (int i = 0; i < reseau.stations.taille(); i++)
    {
        os << reseau.stations[i] << endl;
    }

    return os;
}

istream &operator>>(istream &is, Reseau &reseau)
{
    while (is)
    {
        Station s;
        is >> s;
        if (!is.fail())
        {
            reseau.stations.ajouter(s);
        }
    }
    return is;
}
